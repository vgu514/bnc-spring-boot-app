package com.edgenda.bnc.skillsmanager.service;

import com.edgenda.bnc.skillsmanager.model.Employee;
import com.edgenda.bnc.skillsmanager.model.Skill;
import com.edgenda.bnc.skillsmanager.repository.EmployeeRepository;
import com.edgenda.bnc.skillsmanager.repository.SkillRepository;
import com.edgenda.bnc.skillsmanager.service.exception.UnknownSkillException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RestController
@RequestMapping("/skills")
public class SkillService {

    private final SkillRepository skillRepository;

    private final EmployeeRepository employeeRepository;

    @Autowired
    public SkillService(SkillRepository skillRepository, EmployeeRepository employeeRepository) {
        this.skillRepository = skillRepository;
        this.employeeRepository = employeeRepository;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Skill getSkill(@PathVariable Long id) {
        Assert.notNull(id, "Skill ID cannot be null");
        return skillRepository.findById(id)
                .orElseThrow(() -> new UnknownSkillException(id));
    }

    @RequestMapping
    public List<Skill> getSkills() {
        return skillRepository.findAll();
    }

    @RequestMapping(path="/{skillId}/employees",method = RequestMethod.GET)
    public List<Employee> getEmployeesWithSkill(@PathVariable Long skillId) {
        Assert.notNull(skillId, "Skill ID cannot be null");
        return employeeRepository.findBySkillId(skillId);
    }
}

